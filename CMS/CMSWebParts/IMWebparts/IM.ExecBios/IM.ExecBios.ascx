<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_IMWebparts_IM_ExecBios_IM_ExecBios" CodeBehind="~/CMSWebParts/IMWebparts/IM.ExecBios/IM.ExecBios.ascx.cs" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script>
    //$(document).ready(function () {
    //    $('[data-toggle="popover"]').popover();

    //});
    //.show-content {
    //    display: block;
    //}
    //.hide-content {
    //    display: none;
    //}
    function ShowContent() {




        $(".title-content").toggle(
      function () {
          $(this).addClass("hide-content");

      }, function () {
          $(this).addClass("show-content");
      });

        //$("." + classname).show();
        ////  $("a.text-content").next().css("background-color", "red");


        //for (var i = 1; i < 4; i++) {
        //    $(".text-content" + i).hide();
        //}
        //$("." + classname).show();
    }



    //    popoverOptions = {
    //        content: function () {
    //            return $(this).siblings('.text-content').html();
    //        },
    //        trigger: 'click',
    //        animation: true,
    //        placement: 'bottom'
    //    };
    //    $('.aContent').popover(popoverOptions);

    //});

    //$('.text-content').popover('toggle');





</script>
    <style>
        /*
        * Base structure
        */

        /* Move down content because we have a fixed navbar that is 50px tall */
        body {
            padding-top: 50px;
            height: 100%;
            width: 100%;
        }


        /*
        * Global add-ons
        */

        .sub-header {
            padding-bottom: 10px;
            border-bottom: 1px solid #eee;
        }

        /*
        * Top navigation
        * Hide default border to remove 1px line.
        */
        .navbar-fixed-top {
            border: 0;
        }

        /*
        * Sidebar
        */

        /* Hide for mobile, show later */
        .sidebar {
            display: none;
        }

        @media (min-width: 768px) {
            .sidebar {
                position: fixed;
                top: 51px;
                bottom: 0;
                left: 0;
                z-index: 1000;
                display: block;
                padding: 20px;
                overflow-x: hidden;
                overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
                background-color: #f5f5f5;
                border-right: 1px solid #eee;
            }
        }

        /* Sidebar navigation */
        .nav-sidebar {
            margin-right: -21px; /* 20px padding + 1px border */
            margin-bottom: 20px;
            margin-left: -20px;
        }

            .nav-sidebar > li > a {
                padding-right: 20px;
                padding-left: 20px;
            }

            .nav-sidebar > .active > a,
            .nav-sidebar > .active > a:hover,
            .nav-sidebar > .active > a:focus {
                color: #fff;
                background-color: #428bca;
            }


        /*
        * Main content
        */

        .main {
            padding: 20px;
        }

        @media (min-width: 768px) {
            .main {
                padding-right: 40px;
                padding-left: 40px;
            }
        }

        .main .page-header {
            margin-top: 0;
        }


        /*
        * Placeholder dashboard ideas
        */

        .placeholders {
            margin-bottom: 30px;
            text-align: center;
        }

            .placeholders h4 {
                margin-bottom: 0;
            }

        .placeholder {
            margin-bottom: 20px;
        }

            .placeholder img {
                display: inline-block;
                border-radius: 50%;
            }



        #dvtest {
            width: auto;
            height: 500px;
            margin-left: 20px;
        }

        #dvcontent {
            margin-top: 120px;
            display: inline-block;
            margin-left: 40px;
        }

        .title-align {
            margin-top: 180px;
            font-family: serif;
            margin-left: 60px;
        }

        .content-align {
            margin-top: 10px;
            font-family: serif;
            margin-left: 65px;
            width: 290px;
            line-height: 23px;
        }






        /*STYLES START HERE*/
        .imgAlign {
            height: 200px;
            width: 100%;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        }

        div.card {
            width: 95%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
            margin: 2%;
        }

        div.header {
            color: white;
            font-size: 40px;
        }

        div.container {
            padding: 10px;
        }


        .image-content {
            height: 130px;
            text-align: start;
            margin-left: 10px;
            line-height: 1.9;
        }

        .text-content {
            margin-top: 5%;
            height: 30px;
        }

        .h4, h4 {
            line-height: inherit;
        }

        .aContent {
            float: left;
            margin-left: 5%;
        }

        .padding-0 {
            padding-right: 0;
            padding-left: 0;
        }

        /*.popover {
            top: 395.774px !important;
            left: 5.7778px !important;
        }

        .popover-content {
            background-color: black !important;
            color: white !important;
            width: 535%;
            height: auto;
        }

        .popover-content {
            column-count: 2;
            column-gap: 40px;
        }

        .popover.bottom > .arrow:after {
            border-bottom-color: #000;
        }*/



        /*STYLES ENDS HERE*/


        /*STYLES FOR DEMO*/
        .title-content {
            column-count: 2;
            column-gap: 40px;
            background-color: black !important;
            color: white !important;
            width: 97%;
            height: 270px;
            text-align: left;
            margin-left: 22px;
            margin-top: 25px;
            display: inline-block;
        }

        .popover {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-style: normal;
            font-weight: 400;
            line-height: 1.42857143;
            line-break: auto;
            text-align: left;
            text-align: start;
            text-decoration: none;
            text-shadow: none;
            text-transform: none;
            letter-spacing: normal;
            word-break: normal;
            word-spacing: normal;
            word-wrap: normal;
            white-space: normal;
            font-size: 14px;
        }

        .arrownew {
            position: absolute;
            display: block;
            width: 1rem;
            height: .5rem;
            margin: 0 .3rem;
        }

        .show-content {
            display: block;
        }

        .hide-content {
            display: none;
        }

        .title-content {
            display: none;
        }
        /*.text-content1 {
            display: none;
        }

        .text-content2 {
            display: none;
        }

        .text-content3 {
            display: none;
        }

        .text-content4 {
            display: none;
        }*/
        /*STYLES FOR DEMO ENDS*/
    </style>


<div id="ExecDetail3" runat="server"></div>
<div id="ExecDetail4" runat="server"></div>
  <div class="col-md-12 content">
        <div class="col-md-3 padding-0">
            <div class="card">
                <div class="header">
                    <img src="http://localhost:8010/Kentico12_1/Images/BannerImage.aspx?width=1265&height=440&ext=.jpg" class="imgAlign" />
                </div>
                <div class="image-content">
                    <div>
                        <h4>Alan Monie</h4>
                        <label>Chief Executive Office 123</label>
                    </div>
                </div>
                <a href="#" onclick="ShowContent()" class="aContent">Read Bio</a>
                <div class="text-content">
                </div>

            </div>
        </div>
        <div class="col-md-3 padding-0">
            <div class="card">
                <div class="header">
                    <img src="http://localhost:8010/Kentico12_1/Images/BannerImage.aspx?width=1265&height=440&ext=.jpg" class="imgAlign" />
                </div>
                <div class="image-content">
                    <h4>Augusto P.Aragone</h4>
                    <label>Executive Vice President, Secretary and General Counsel</label>
                </div>
                <a href="#" onclick="ShowContent()" class="aContent">Read Bio</a>
                <div class="text-content">
                </div>

            </div>
        </div>
        <div class="col-md-3 padding-0">
            <div class="card">
                <div class="header">
                    <img src="http://localhost:8010/Kentico12_1/Images/BannerImage.aspx?width=1265&height=440&ext=.jpg" class="imgAlign" />
                </div>
                <div class="image-content">
                    <h4>Scott D. Sherman</h4>
                    <label>Executive Vice President, Human Resources</label>

                </div>
                <a href="#" data-toggle="tooltip" data-placement="bottom" class="aContent" title="Hooray!">Read Bio</a>
                <div class="text-content">
                </div>

            </div>
        </div>
        <div class="col-md-3 padding-0">
            <div class="card">
                <div class="header">
                    <img src="http://localhost:8010/Kentico12_1/Images/BannerImage.aspx?width=1265&height=440&ext=.jpg" class="imgAlign" />
                </div>
                <div class="image-content">
                    <h4>Paul Bay</h4>
                    <label>Executive Vice President and President Global Technology Solutions</label>

                </div>
                <a href="#" data-toggle="tooltip" data-placement="bottom" class="aContent" title="Hooray!">Read Bio</a>
                <div class="text-content">
                </div>

            </div>
        </div>
    </div>
    <div class="arrownew"></div>
    <div class="title-content">
        Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
                                    Jump to Example: Enable popovers everywhere - Overview. Example: Enable popovers everywhere. Example: Using the container option. Example. Four directions. Dismiss on next click. Specific markup required for dismiss-on-next-click. Disabled elements. Usage. Options. Data attributes for individual popovers. Methods. Asynchronous methods and transitions popover
    </div>



