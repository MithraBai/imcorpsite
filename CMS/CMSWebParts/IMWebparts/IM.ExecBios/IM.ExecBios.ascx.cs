using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.Helpers;
using CMS.PortalEngine.Web.UI;

public partial class CMSWebParts_IMWebparts_IM_ExecBios_IM_ExecBios : CMSAbstractWebPart
{
    #region "Properties"

    #region ExecBio1
    public string ExecName1
    {
        get { return ValidationHelper.GetString(GetValue("ExecName1"), string.Empty); }
        set { SetValue("ExecName1", value); }
    }
    public string ExecImage1
    {
        get { return ValidationHelper.GetString(GetValue("ExecImage1"), string.Empty); }
        set { SetValue("ExecImage1", value); }
    }
    public string ExecTitle1
    {
        get { return ValidationHelper.GetString(GetValue("ExecTitle1"), string.Empty); }
        set { SetValue("ExecTitle1", value); }
    }
    public string ExecBio1
    {
        get { return ValidationHelper.GetString(GetValue("ExecBio1"), string.Empty); }
        set { SetValue("ExecBio1", value); }
    }
    public string ExecBiolink1
    {
        get { return ValidationHelper.GetString(GetValue("ExecBiolink1"), string.Empty); }
        set { SetValue("ExecBiolink1", value); }
    }
    public string HideControl
    {
        get
        {
            return ValidationHelper.GetString(GetValue("Visible"), string.Empty);
        }
        set
        {
            SetValue("Visible", value);
        }
    }
    #endregion

    #region ExecBio2
    public string ExecName2
    {
        get { return ValidationHelper.GetString(GetValue("ExecName2"), string.Empty); }
        set { SetValue("ExecName2", value); }
    }

    public string ExecImage2
    {
        get { return ValidationHelper.GetString(GetValue("ExecImage2"), string.Empty); }
        set { SetValue("ExecImage2", value); }
    }
    public string ExecTitle2
    {
        get { return ValidationHelper.GetString(GetValue("ExecTitle2"), string.Empty); }
        set { SetValue("ExecTitle2", value); }
    }
    public string ExecBio2
    {
        get { return ValidationHelper.GetString(GetValue("ExecBio2"), string.Empty); }
        set { SetValue("ExecBio2", value); }
    }
    public string ExecBiolink2
    {
        get { return ValidationHelper.GetString(GetValue("ExecBiolink2"), string.Empty); }
        set { SetValue("ExecBiolink2", value); }
    }
    #endregion


    #region ExecBio3
    public string ExecName3
    {
        get { return ValidationHelper.GetString(GetValue("ExecName3"), string.Empty); }
        set { SetValue("ExecName3", value); }
    }

    public string ExecImage3
    {
        get { return ValidationHelper.GetString(GetValue("ExecImage3"), string.Empty); }
        set { SetValue("ExecImage3", value); }
    }
    public string ExecTitle3
    {
        get { return ValidationHelper.GetString(GetValue("ExecTitle3"), string.Empty); }
        set { SetValue("ExecTitle3", value); }
    }
    public string ExecBio3
    {
        get { return ValidationHelper.GetString(GetValue("ExecBio3"), string.Empty); }
        set { SetValue("ExecBio3", value); }
    }
    public string ExecBiolink3
    {
        get { return ValidationHelper.GetString(GetValue("ExecBiolink3"), string.Empty); }
        set { SetValue("ExecBiolink3", value); }
    }
    #endregion


    #region ExecBio4
    public string ExecName4
    {
        get { return ValidationHelper.GetString(GetValue("ExecName4"), string.Empty); }
        set { SetValue("ExecName4", value); }
    }

    public string ExecImage4
    {
        get { return ValidationHelper.GetString(GetValue("ExecImage4"), string.Empty); }
        set { SetValue("ExecImage4", value); }
    }
    public string ExecTitle4
    {
        get { return ValidationHelper.GetString(GetValue("ExecTitle4"), string.Empty); }
        set { SetValue("ExecTitle4", value); }
    }
    public string ExecBio4
    {
        get { return ValidationHelper.GetString(GetValue("ExecBio4"), string.Empty); }
        set { SetValue("ExecBio4", value); }
    }
    public string ExecBiolink4
    {
        get { return ValidationHelper.GetString(GetValue("ExecBiolink4"), string.Empty); }
        set { SetValue("ExecBiolink4", value); }
    }
    #endregion


    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            
            ExecDetail3.Visible = !string.IsNullOrWhiteSpace(ExecName3);
            ExecDetail4.Visible = !string.IsNullOrWhiteSpace(ExecName4);


        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}