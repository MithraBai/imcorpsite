<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="CMSWebParts_IMWebparts_IM_PageHero"
    CodeBehind="~/CMSWebParts/IMWebparts/IM.PageHero.ascx.cs" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script>
</script>
<style>
    body {
            width: 100%;
            height: 100%;
            overflow: auto;
        }

        .imagecontent {
            width: 100%;
        }

        .dvImage {
            background-size: cover;
            height: 100%;
            width: 100%;
            height: 458px;
            position: relative;
            margin-bottom: 8px;
            background-size: cover !important;
            background-color: rgb(255, 255, 255) !important;
            background-repeat: no-repeat !important;
            background-position: 50% center !important;
        }

        .dvImageContent {
            flex-direction: row;
            top: 20%;
            width: 100%;
            height: 53%;
            padding: 60px;
            position: absolute;
            text-align: left;
            align-items: center;
            justify-content: center;
        }

        .content-align {
            height: 109px;
            width: 357px;
            color: #FFFFFF;
            font-family: "Helvetica Neue";
            font-size: 16px;
            letter-spacing: 0;
            line-height: 24px;
        }

        .title-align {
            height: 100%;
            width: 100%;
        }

    @media only screen and (min-width: 380px) and (max-width: 992px) {

        .dvImageContent {
            margin: 0px auto;
        }
    }
    @media only screen and (min-width: 320px) and (max-width: 568px) {
            .content-align {
                width: auto;
            }
        }
    .text-color-white{
        color:#FFFFFF;
    }
</style>
 <div class="">
        <div class="row">
            <div class="imagecontent">
                <div id="bgImage" class="dvImage" runat="server">
                    <div class="dvImageContent">
                        <div class="text-color-white" >
                            <h1 class="title-align"><%=Title %></h1>
                            <h5 class="content-align"><%= Description %></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

