using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.Helpers;
using CMS.PortalEngine.Web.UI;

public partial class CMSWebParts_IMWebparts_IM_ImageRightTextLeft_IM_ImageRightTextLeft : CMSAbstractWebPart
{
    #region "Properties"
   
    public string BlockRightImage
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BlockRightImage"), string.Empty);
        }
        set
        {
            SetValue("BlockRightImage", value);
        }
    }

    public string BlockRightImageDescription
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BlockRightImageDescription"), string.Empty);
        }
        set
        {
            SetValue("BlockRightImageDescription", value);
        }
    }

    public string BlockLeftTitle
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BlockLeftTitle"), string.Empty);
        }
        set
        {
            SetValue("BlockLeftTitle", value);
        }
    }

    public string BlockLeftDescription
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BlockLeftDescription"), string.Empty);
        }
        set
        {
            SetValue("BlockLeftDescription", value);
        }
    }

    public string BlockLeftButtonUrl1
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BlockLeftButtonUrl1"), string.Empty);
        }
        set
        {
            SetValue("BlockLeftButtonUrl1", value);
        }
    }

    public string BlockLeftButtonText1
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BlockLeftButtonText1"), string.Empty);
        }
        set
        {
            SetValue("BlockLeftButtonText1", value);
        }
    }

   
    public string BlockLeftButtonUrl2
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BlockLeftButtonUrl2"), string.Empty);
        }
        set
        {
            SetValue("BlockLeftButtonUrl2", value);
        }
    }

    public string BlockLeftButtonText2
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BlockLeftButtonText2"), string.Empty);
        }
        set
        {
            SetValue("BlockLeftButtonText2", value);
        }
    }

    public string Imageurl = string.Empty;

    public string HideControl
    {
        get
        {
            return ValidationHelper.GetString(GetValue("Visible"), string.Empty);
        }
        set
        {
            SetValue("Visible", value);
        }
    }


    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        Imageurl = URLHelper.GetAbsoluteUrl(URLHelper.ResolveUrl(BlockRightImage.Trim()));
        if (!StopProcessing)
        {
            

                if (string.IsNullOrEmpty(BlockRightImage))
                {
                    blockRightImage.Visible = false;
                }
                else
                {
                
                //blockRightImage.Attributes["data-src"] = Imageurl;
                blockRightImage.Attributes["src"] = Imageurl;
                    blockRightImage.Alt = BlockRightImageDescription;
                }
         

           

            if (string.IsNullOrEmpty(BlockLeftButtonUrl1))
            {
                blockLeftButton1.Visible = false;
            }
            else
            {
                blockLeftButton1.HRef = BlockLeftButtonUrl1;
                blockLeftButton1.InnerText = BlockLeftButtonText1;
            }

            //if (BlockRightButtonTarget1)
            //{
            //    blockRightButton1.Target = "_blank";
            //}


            if (string.IsNullOrEmpty(BlockLeftButtonUrl2))
            {
                blockLeftButton2.Visible = false;
            }
            else
            {
                blockLeftButton2.HRef = BlockLeftButtonUrl2;
                blockLeftButton2.InnerText = BlockLeftButtonUrl2;
            }

           
        }
        else
        {

        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}