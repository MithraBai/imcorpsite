<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_IMWebparts_IM_ImageRightTextLeft_IM_ImageRightTextLeft" CodeBehind="~/CMSWebParts/IMWebparts/IM.ImageRightTextLeft/IM.ImageRightTextLeft.ascx.cs" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="C:\inetpub\wwwroot\Kentico12\CMS\App_Themes\CorporateSite" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<section class="section">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-middle" runat="server">
            <div class="cell medium-6 small-order-2 medium-order-1">
                <h3 class="section__title"><%=BlockLeftTitle %></h3> <%=Imageurl %>
               <%=BlockLeftDescription %>
                <div class="margin-top-2">
                    <a id="blockLeftButton1" runat="server" class="button quaternary margin-right-1"></a>
                    <a id="blockLeftButton2" runat="server" class="button"></a>
                </div>
            </div>
            <div class="cell medium-6 small-order-1 medium-order-2">
                <img id="blockRightImage"  runat="server" />
               
            </div>
        </div>
    </div>
</section>