using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.Helpers;
using CMS.PortalEngine.Web.UI;

public partial class CMSWebParts_IMWebparts_IM_PageHero : CMSAbstractWebPart
{
    #region "Properties"
    public string Title
    {
        get { return ValidationHelper.GetString(GetValue("Title"), string.Empty); }
        set { SetValue("Title", value); }
    }

    public string Description
    {
        get { return ValidationHelper.GetString(GetValue("Description"), string.Empty); }
        set { SetValue("Description", value); }
    }
    public string BackgroundImage
    {
        get { return ValidationHelper.GetString(GetValue("BackgroundImage"), string.Empty); }
        set { SetValue("BackgroundImage", value); }
    }
    public string Alttext
    {
        get { return ValidationHelper.GetString(GetValue("Alt"), string.Empty); }
        set { SetValue("Alt", value); }
    }
    public string Imageurl = string.Empty;
    public string HideControl
    {
        get
        {
            return ValidationHelper.GetString(GetValue("Visible"), string.Empty);
        }
        set
        {
            SetValue("Visible", value);
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            Imageurl = URLHelper.GetAbsoluteUrl(URLHelper.ResolveUrl(BackgroundImage.Trim()));
            bgImage.Attributes.CssStyle.Add("background-image", Imageurl);
            bgImage.Attributes.CssStyle.Add("background-repeat", "no-repeat");
            //dvtest.Attributes.CssStyle.Add("background-color", "red");
            bgImage.Attributes.Add("aria-label" ,Alttext);

            //bgImage.Attributes.CssStyle.Add("width", "auto");
            //bgImage.Attributes.CssStyle.Add("height", "500px");
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}